<?php

echo json_encode(
    $data = array(
        [
            'id' => 1,
            'latitude' => 23.772402,
            'longitude' => -99.165613,
            'title' => "Adrián Rodríguez",
            'location' => "Ruta 1",
            'city' => 1,
            'phone' => "(834)-165-9002",
            'category' => "Tanqueta",
            'rating' => "4",
            'reviews_number' => "6",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"1",
            'ruta'=>"1.1",
            'dias'=>"LUN-MAR-MIER-JUE-VIE-SAB",
            'turno'=>"VESPERTINO",
            'chofer'=>"Hector <br>Guadalupe Grimaldo Torres",
            'imagen'=>"assets/img/items/HectorGuadalupeGrimaldoTorres.jpg",
            'fb_id'=>"-KmSsMyCGsM7l3KfpXjl",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),

            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'opening_hours' => array(
                "00:00am - 11:00pm",
                "14:00am - 19:00pm",
                "10:00am - 11:00pm",
                "14:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            ),
        ],

        [
            'id' => 2,
            'latitude' => 23.772461,
            'longitude' => -99.149176,
            'title' => "Ironapple",
            'location' => "4209 Glenview Drive",
            'city' => 1,
            'contact' => "989-410-0777",
            'category' => "Camión",
            'rating' => "3",
            'reviews_number' => "12",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"1",
            'ruta'=>"1.2",
            'dias'=>"LUN-MAR-MIER-JUE-VIE-SAB",
            'turno'=>"VESPERTINO",
            'chofer'=>"Antonio <br>Alvarez Alcocer",
            'imagen'=>"assets/img/items/AntonioAlvarezAlcocer.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'today_menu' => array(
                [
                    'meal_type' => "Starter",
                    'meal' => "Smoked Salmon, Classic Condiments, Brioche"
                ],
                [
                    'meal_type' => "Soup",
                    'meal' => "Roasted Golden Beets, Goat Cheese, Hazelnut Granola"
                ],
                [
                    'meal_type' => "Main course",
                    'meal' => "Napoleon of Rabbit Loin, Braised Leek, Fava Bean Puree"
                ]
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 3,
            'latitude' => 23.768504,
            'longitude'=> -99.129392,
            'title' => "Food Festival",
            'location' => "23 Hillhaven Drive",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>323-843-4729",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "15",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"1",
            'ruta'=>"1.3",
            'dias'=>"LUN-MAR-MIER-JUE-VIE-SAB",
            'turno'=>"VESPERTINO",
            'chofer'=>"Lorenzo <br>Hernández Córdoba",
            'imagen'=>"assets/img/items/LorenzoHernandezCordoba.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'ribbon' => "",
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
        ],

        [
            'id' => 4,
            'latitude' => 23.755802,
            'longitude' => -99.164750,
            'title' => "Cosmopolit",
            'location' => "4209 Glenview Drive",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>323-843-4729",
            'category' => "Camión",
            'rating' => "5",
            'reviews_number' => "28",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"1",
            'ruta'=>"1.4",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Alberto <br>Rodríguez Núñez",
            'imagen'=>"assets/img/items/AlbertoRodriguezNunez.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'opening_hours' => array(
                "08:00am - 11:00pm",
                "08:00am - 11:00pm",
                "12:00am - 11:00pm",
                "08:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            )
        ],

        [
            'id' => 5,
            'latitude' => 23.749429,
            'longitude' => -99.135450,
            'title' => "Beautiful Luxury Villa",
            'location' => "3284 Hillside Street",
            'city' => 2,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Camión",
            'rating' => "",
            'reviews_number' => "",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"1",
            'ruta'=>"1.5",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Joel Lopez",
            'imagen'=>"assets/img/items/",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'tags' => array(
                "Bedroom",
                "Parking",
                "TV",
                "Bathroom"
            ),
            'ribbon' => "",
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'description_list' =>array(
                [
                    'title' => "Bathrooms",
                    'value' => 3,
                ],
                [
                    'title' => "Bedrooms",
                    'value' => 4,
                ],
                [
                    'title' => "Area",
                    'value' => "458m<sup>2</sup>",
                ],
                [
                    'title' => "Garages",
                    'value' => 2,
                ],
                [
                    'title' => "Status",
                    'value' => "Sale",
                ],
            ),
        ],

        [
            'id' => 6,
            'latitude' => 23.744872,
            'longitude'=> -99.126985,
            'title' => "Stand Up Show",
            'location' => "534 Sycamore Road",
            'city' => 2,
            'contact' => "<i class='fa fa-phone'></i>352-383-7435",
            'category' => "Tanqueta",
            'rating' => "4",
            'reviews_number' => "8",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"1",
            'ruta'=>"1.6",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Homero <br>Bocanegra Grimaldo",
            'imagen'=>"assets/img/items/HomeroBocanegraGrimaldo.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
        ],

        [
            'id' => 7,
            'latitude' => 23.741062,
            'longitude' => -99.108252,
            'title' => "University Day",
            'location' => "Central Town University",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>925-585-2459",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "10",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"1",
            'ruta'=>"1.7",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Apolinar <br>Villela Perez",
            'imagen'=>"assets/img/items/ApolinarVillelaPerez.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'video' => "<iframe src='https://player.vimeo.com/video/184360631' width='640' height='360' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"

        ],

        [
            'id' => 8,
            'latitude' => 23.736544,
            'longitude' => -99.127843,
            'title' => "City Tour",
            'location' => "Downtown center",
            'city' => 1,
            'contact' => "<i class='fa fa-envelope'></i>reservation@citytours.com",
            'category' => "Camión",
            'rating' => "5",
            'reviews_number' => "17",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"1",
            'ruta'=>"1.8",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Miguel <br>Ángel García Mata",
            'imagen'=>"assets/img/items/MiguelAngelGarciaMata.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "12:00",
                    'location_title' => "Town Square",
                    'location_address' => ""
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => ""
                ]
            ),
        ],

        [
            'id' => 9,
            'latitude' => 23.732223,
            'longitude' => -99.137767,
            'title' => "High Mountain Trip",
            'location' => "East Alps",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>hello@mountaintrip.com",
            'category' => "Tanqueta",
            'rating' => "4",
            'reviews_number' => "9",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"1",
            'ruta'=>"1.9",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Eladio <br>Vazquez Cárdenas",
            'imagen'=>"assets/img/items/EladioVazquezCardenas.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'tags' => array(
                "Adrenaline",
                "Skialp",
                "Climbing",
                "Tourism"
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
        ],

        [
            'id' => 10,
            'latitude' => 23.728687,
            'longitude' => -99.152873,
            'title' => "Hyundai i30",
            'location' => "580 Briarhill Lane",
            'city' => 1,
            'phone' => "325-990-8452",
            'category' => "Camión",
            'rating' => "",
            'reviews_number' => "",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"2",
            'ruta'=>"2.1",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Luis javier <br>Mireles Cabrera",
            'imagen'=>"assets/img/items/LuisJavierMirelesCabrera.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'tags' => array(
                "Diesel",
                "First Owner",
                "4x4",
                "Air Conditioning"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'description_list' =>array(
                [
                    'title' => "Engine",
                    'value' => "Diesel",
                ],
                [
                    'title' => "Mileage",
                    'value' => 14500,
                ],
                [
                    'title' => "Max Speed",
                    'value' => "220 Mph",
                ],
                [
                    'title' => "Color",
                    'value' => "Dark Brown",
                ],
                [
                    'title' => "Status",
                    'value' => "Sale",
                ],
            ),
        ],

        [
            'id' => 11,
            'latitude' => 23.728098,
            'longitude' => -99.157293,
            'title' => "Thai Massage",
            'location' => "1360 Meadowbrook Mall Road",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>+21 310-877-5920",
            'category' => "Camión",
            'rating' => "5",
            'reviews_number' => "23",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"2",
            'ruta'=>"2.10",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Juan pablo <br>Martinez Báez",
            'imagen'=>"assets/img/items/JuanPabloMartinezBaez.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'ribbon' => "",
            'velocidad' => "0.0",
            'url' => "",
            'description' => "10.0kmn"
        ],

        [
            'id' => 12,
            'latitude' => 23.725397,
            'longitude' => -99.162990,
            'title' => "Senior C# Developer",
            'location' => "ERF Solutions",
            'city' => 1,
            'contact' => "<i class='fa fa-envelope'></i>developers@erf.com",
            'tags' => array(
                "Java",
                "C#",
                "Developer",
                "Big Company",
                "Benefits"
            ),
            'category' => "Camión",
            'rating' => "",
            'reviews_number' => "",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"2",
            'ruta'=>"2.11",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Epifanío <br>Mireles cabrera",
            'imagen'=>"assets/img/items/EpifanioMirelesCabrera.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'description' => "10.0km "
        ],

        [
            'id' => 13,
            'latitude' => 23.719022,
            'longitude' => -99.166316,
            'title' => "The Brooklyns & Hosts",
            'location' => "Town Hall",
            'city' => 1,
            'contact' => "<i class='fa fa-envelope'></i>reservations@tickets.com",
            'category' => "Camión",
            'rating' => "5",
            'reviews_number' => "17",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"2",
            'ruta'=>"2.12",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Pedro <br>Becerra Moctezuma",
            'imagen'=>"assets/img/items/PedroBecerraMoctezuma.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
        ],

        [
            'id' => 14,
            'latitude' => 23.707500,
            'longitude' => -99.151607,
            'title' => "How to Draw Better",
            'location' => "Central Gallery",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>402-565-1871",
            'category' => "Camión",
            'rating' => "3",
            'reviews_number' => "12",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"2",
            'ruta'=>"2.13",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Jorge armando <br>Muñoz Muñiz",
            'imagen'=>"assets/img/items/JorgeArmandoMunozMuniz.jpg",
            'gallery' => array(
                "assets/img/camion1.jpg",
                "assets/img/camion2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 15,
            'latitude' => 23.710103,
            'longitude' => -99.135514,
            'title' => "Bambi Planet Houseboat Bar & Grill",
            'location' => "White River Dock",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "4",
            'reviews_number' => "6",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.14",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Aurelio Zuñiga",
            'imagen'=>"assets/img/items/",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'today_menu' => array(
                [
                    'meal_type' => "Starter",
                    'meal' => "Smoked Salmon, Classic Condiments, Brioche"
                ],
                [
                    'meal_type' => "Soup",
                    'meal' => "Roasted Golden Beets, Goat Cheese, Hazelnut Granola"
                ],
                [
                    'meal_type' => "Main course",
                    'meal' => "Napoleon of Rabbit Loin, Braised Leek, Fava Bean Puree"
                ]
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'opening_hours' => array(
                "08:00am - 11:00pm",
                "08:00am - 11:00pm",
                "12:00am - 11:00pm",
                "08:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            )
        ],

        [
            'id' => 16,
            'latitude' => 23.709818,
            'longitude' => -99.128905,
            'title' => "Paris Trip",
            'location' => "Saturn Hotel Eiffel Square",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "9",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.2",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Luis Gerardo <br>Maldonado Campos",
            'imagen'=>"assets/img/items/LuisGerardoMaldonadoCampos.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 17,
            'latitude' => 23.705240,
            'longitude' => -99.129592,
            'title' => "Big Pizza for Couples",
            'location' => "Riccardo Pizza House",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "4",
            'reviews_number' => "7",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.3",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"José Francisco <br>Rodríguez Maldonado",
            'imagen'=>"assets/img/items/JoseFranciscoRodriguezMaldonado.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'opening_hours' => array(
                "08:00am - 11:00pm",
                "08:00am - 11:00pm",
                "12:00am - 11:00pm",
                "08:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 18,
            'latitude' => 23.711448,
            'longitude' => -99.181884,
            'title' => "Healthy Breakfast",
            'location' => "Mom's Kitchen",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "12",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.4",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Artemio <br>Rodríguez Guillén",
            'imagen'=>"assets/img/items/ArtemioRodriguezGuillen.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'opening_hours' => array(
                "08:00am - 11:00pm",
                "08:00am - 11:00pm",
                "12:00am - 11:00pm",
                "08:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            ),
            'today_menu' => array(
                [
                    'meal_type' => "Starter",
                    'meal' => "Smoked Salmon, Classic Condiments, Brioche"
                ],
                [
                    'meal_type' => "Soup",
                    'meal' => "Roasted Golden Beets, Goat Cheese, Hazelnut Granola"
                ],
                [
                    'meal_type' => "Main course",
                    'meal' => "Napoleon of Rabbit Loin, Braised Leek, Fava Bean Puree"
                ]
            ),
        ],

        [
            'id' => 19,
            'latitude' => 23.712489,
            'longitude' => -99.186229,
            'title' => "Coffee Break",
            'location' => "35 High Street",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "3",
            'reviews_number' => "6",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.5",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Alberto <br>Mandujano Gomez",
            'imagen'=>"assets/img/items/AlbertoMandujanoGomez.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'opening_hours' => array(
                "08:00am - 11:00pm",
                "08:00am - 11:00pm",
                "12:00am - 11:00pm",
                "08:00am - 11:00pm",
                "03:00pm - 02:00am",
                "03:00pm - 02:00am",
                "Closed"
            ),
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 20,
            'latitude' => 23.723657,
            'longitude' => -99.160920,
            'title' => "Weekend in Venice",
            'location' => "Venice Hotel****",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "23",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.6",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Samuel Isai <br>Rios Rico",
            'imagen'=>"assets/img/items/SamuelIsaiRiosRico.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
        ],

        [
            'id' => 21,
            'latitude' => 23.732526,
            'longitude' => -99.165201,
            'title' => "Tree Therapy",
            'location' => "Eastpark Forest",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "6",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.7",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"VESPERTINO",
            'chofer'=>"Urbano <br>Yáñez Cano",
            'imagen'=>"assets/img/items/UrbanoYanezCano.jpg",
           'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
        ],

        [
            'id' => 22,
            'latitude' => 23.747454,
            'longitude' => -99.165866,
            'title' => "Gourman Festival",
            'location' => "Station Square",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>",
            'category' => "Tanqueta",
            'rating' => "",
            'reviews_number' => "0",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.8",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Domingo <br>Limon Olvera",
            'imagen'=>"assets/img/items/DomingoLimonOlvera.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
            'tags' => array(
                "Wi-Fi",
                "Parking",
                "TV",
                "Vegetarian"
            ),
        ],

        [
            'id' => 23,
            'latitude' => 23.751510,
            'longitude'=> -99.178569,
            'title' => "Nascar Racing",
            'location' => "London Airport",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>541-435-6211",
            'category' => "Tanqueta",
            'rating' => "5",
            'reviews_number' => "11",
            'marker_image' => "assets/img/Tanqueta.png",
            'sector'=>"2",
            'ruta'=>"2.9",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Juan <br>Paulin Mandujano Gomez",
            'imagen'=>"assets/img/items/JuanPaulinMadujanoGomez.jpg",
            'gallery' => array(
                "assets/img/tanqueta1.jpg",
                "assets/img/tanqueta2.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "detail.html",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Century Cup",
                    'location_address' => "Nascar Ring"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "24/7 Rally",
                    'location_address' => "Nascar Ring"
                ]
            ),
        ],

        [
            'id' => 24,
            'latitude' => 23.755772,
            'longitude' => -99.180929,
            'title' => "Golf Lessons",
            'location' => "49 Lance Golf Club",
            'city' => 1,
            'contact' => "<i class='fa fa-phone'></i>+1 317-598-2912",
            'category' => "Camion",
            'rating' => "5",
            'reviews_number' => "17",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"3",
            'ruta'=>"3.1",
            'dias'=>"LUNES-MARTES-MIERCOLES-JUEVES-VIERNES-SABADO",
            'turno'=>"MATUTINO",
            'chofer'=>"José Adrián <br>Rodríguez Quintero",
            'imagen'=>"assets/img/items/JoseAdrianRodriguezQuintero.jpg",
            'gallery' => array(
                "assets/img/items/14.jpg",
                "assets/img/items/6.jpg",
                "assets/img/items/9.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'schedule' => array(
                [
                    'date' => "24.03.2017",
                    'time' => "16:00",
                    'location_title' => "Town Square",
                    'location_address' => "458 Brigth Street London"
                ],
                [
                    'date' => "03.04.2017",
                    'time' => "18:00",
                    'location_title' => "Bristol Gallery",
                    'location_address' => "87 Yellow Lane, Manhattan"
                ]
            ),
            'video' => "<iframe src='https://player.vimeo.com/video/16688587?title=0&byline=0&portrait=0' width='640' height='272' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
        ],

        [
            'id' => 25,
            'latitude' => 23.732714,
            'longitude' => -99.142839,
            'title' => "Tennis for Beginners",
            'location' => "Tennis Center",
            'city' => 3,
            'contact' => "<i class='fa fa-phone'></i>+1 317-598-2912",
            'category' => "Camion",
            'rating' => "4",
            'reviews_number' => "17",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"3",
            'ruta'=>"3.10",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Nelson <br>Mejia Reyes",
            'imagen'=>"assets/img/items/NelsonMejiaReyes.jpg",
            'gallery' => array(
                "assets/img/items/8.jpg",
                "assets/img/items/6.jpg",
                "assets/img/items/9.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'video' => ""
        ],

        [
            'id' => 26,
            'latitude' => 23.733185,
            'longitude' => -99.145736,
            'title' => "Red Ear Restaurant",
            'location' => "48 Josh Lane",
            'city' => 3,
            'contact' => "<i class='fa fa-phone'></i>+1 457-48-6846",
            'category' => "Camion",
            'rating' => "5",
            'reviews_number' => "5",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"3",
            'ruta'=>"3.11",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Javier <br>Zurita Hernández",
            'imagen'=>"assets/img/items/JavierZuritaHernandez.jpg",
            'gallery' => array(
                "assets/img/items/3.jpg",
                "assets/img/items/6.jpg",
                "assets/img/items/9.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'video' => ""
        ],

        [
            'id' => 27,
            'latitude' => 23.735513,
            'longitude' => -99.151991,
            'title' => "Paris st. Restaurant",
            'location' => "Rue La Fayette",
            'city' => 4,
            'contact' => "<i class='fa fa-phone'></i>+6 457-48-6846",
            'category' => "Camion",
            'rating' => "5",
            'reviews_number' => "5",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"3",
            'ruta'=>"3.12",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Andrés <br>Cervantes Ruiz",
            'imagen'=>"assets/img/items/AndresCervantesRuiz.jpg",
            'gallery' => array(
                "assets/img/items/6.jpg",
                "assets/img/items/6.jpg",
                "assets/img/items/9.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'video' => ""
        ],

        [
            'id' => 28,
            'latitude' => 23.738165,
            'longitude' => -99.159372,
            'title' => "Manoir Wellness & Spa",
            'location' => "Quai de Valmy",
            'city' => 4,
            'contact' => "<i class='fa fa-phone'></i>+6 3548-189-4455",
            'category' => "Camion",
            'rating' => "5",
            'reviews_number' => "6",
            'marker_image' => "assets/img/Camion.png",
            'sector'=>"3",
            'ruta'=>"3.13",
            'dias'=>"LUNES-MIERCOLES-VIERNES",
            'turno'=>"MATUTINO",
            'chofer'=>"Víctor <br>Del Valle Sosa",
            'imagen'=>"assets/img/items/VictordelValleSosa.jpg",
            'gallery' => array(
                "assets/img/items/2.jpg",
                "assets/img/items/6.jpg",
                "assets/img/items/9.jpg"
            ),
            'velocidad' => "0.0",
            'url' => "",
            'distancia' => "10.0",
            'reviews' => array(
                [
                    'author_name' => "Jane Doe",
                    'author_image' => "assets/img/person-01.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
                ],
                [
                    'author_name' => "Norma Brown",
                    'author_image' => "assets/img/person-02.jpg",
                    'date' => '09.05.2016',
                    'rating' => 4,
                    'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
                ]
            ),
            'video' => ""
        ],

    )
);

