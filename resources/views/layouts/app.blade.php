<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/fonts/elegant-fonts.css') }}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/zabuto_calendar.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('assets/css/trackpad-scroll-emulator.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">

</head>

<body class="homepage">
<div class="page-wrapper">
    <header id="page-header">
        <nav>
            <div class="left">
                <a href="index.html" class="brand"><img src="assets/img/logo.png" alt="" width="133"></a>
            </div>
            <!--end left-->
            <div class="right">
                <div class="primary-nav has-mega-menu">
                    <ul class="navigation">
                        <li class="active has-child"><a href="#nav-homepages">Inicio</a>
                            <div class="wrapper">
                                <div id="nav-homepages" class="nav-wrapper">
                                    <ul>
                                        <li><a href="index-map-version-1.html">Map Full Screen Sidebar Results</a></li>
                                        <li><a href="index-map-version-2.html">Map Horizontal Form</a></li>
                                        <li><a href="index-map-version-3.html">Map Full Screen Form in Sidebar</a></li>
                                        <li><a href="index-map-version-4.html">Map Form Under</a></li>
                                        <li><a href="index-map-version-5.html">Map Sidebar Grid</a></li>

                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li class="has-child"><a href="#nav-locations">Administración</a>
                            <div class="wrapper">
                                <div id="nav-locations" class="nav-wrapper">
                                    <ul>
                                        <li class=""><a href="#nav-locations-new-york">Camiones</a></li>
                                        <li class=""><a href="#nav-locations-new-york">Choferes</a></li>
                                        <li class=""><a href="#nav-locations-new-york">Rutas</a></li>
                                        <li class="has-child"><a href="#nav-6">Reportes</a>
                                            <div class="nav-wrapper" id="nav-6">
                                                <ul>
                                                    <li><a href="#">Louvre</a></li>
                                                    <li><a href="#">Bourse</a></li>
                                                    <li><a href="#">Marais</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--end nav-wrapper-->
                            </div>
                            <!--end wrapper-->
                        </li>
                        <!--<li><a href="contact.html">Contact</a></li>-->
                    </ul>
                    <!--end navigation-->
                </div>
                <!--end primary-nav-->
                @if (Auth::guest())
                    <div class="secondary-nav">
                        <a href="#" data-modal-external-file="modal_sign_in.php" data-target="modal-sign-in"><i class="fa fa-sign-in"></i><span>Sign In</span></a>
                        <a href="#" class="promoted" data-modal-external-file="modal_register.php" data-target="modal-register"><i class="fa fa-user"></i><span>Register</span></a>
                    </div>
                @else
                    <div class="secondary-nav">
                        <ul class="navigation">
                            <li class="has-child">
                                <div class="image"><div class="bg-transfer"><img src="assets/img/person-01.jpg" alt=""></div></div>
                                <a href="#" class="invisible-on-mobile">{{ Auth::user()->name }}</a>
                                <div class="wrapper">
                                    <div class="nav-wrapper">
                                        <ul>
                                            <li><a href="profile.html"><i class="fa fa-user"></i>View Profile</a></li>
                                            <li><a href="reset-password.html"><i class="fa fa-refresh"></i>Change Password</a></li>
                                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    <i class="fa fa-sign-out"></i>Log Out</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
            @endif
            <!--end secondary-nav-->
                <a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" data-modal-external-file="modal_submit.php" data-target="modal-submit"><i class="fa fa-plus"></i><span>Enviar mensaje</span></a>
                <div class="nav-btn">
                    <i></i>
                    <i></i>
                    <i></i>
                </div>
                <!--end nav-btn-->
            </div>
            <!--end right-->
        </nav>
        <!--end nav-->
    </header>
    <!--end page-header-->

    <div id="page-content">
        @yield('content')
    </div>
    <!--end page-content-->

    <footer id="page-footer">
        <div class="footer-wrapper">
            <div class="block">
                <div class="container">
                    <div class="vertical-aligned-elements">

                        <div class="element width-50">
                            <p data-toggle="modal">Presidencia Municipal de Victoria, Administración 2016-2018 Francisco I. Madero 102, Zona Centro. Ciudad Victoria, Tamaulipas. Conmutador (834) 3187800</p>
                        </div>
                        <div class="element width-50 text-align-right">
                            <a href="https://twitter.com/gob_victoria/" class="circle-icon"><i class="social_twitter"></i></a>
                            <a href="https://www.facebook.com/gobvictoria/" class="circle-icon"><i class="social_facebook"></i></a>
                            <a href="https://www.youtube.com/user/GobiernoVictoria/" class="circle-icon"><i class="social_youtube"></i></a>
                        </div>
                    </div>
                    <div class="background-wrapper">
                        <div class="bg-transfer opacity-50">
                            <img src="assets/img/footer-bg.png" alt="">
                        </div>
                    </div>
                    <!--end background-wrapper-->
                </div>
            </div>
            <div class="footer-navigation">
                <div class="container">
                    <div class="vertical-aligned-elements">
                        <div class="element width-50">(C) {{ date("Y") }} Softuex Innovation S.A. de C.V., All right reserved</div>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--end page-footer-->
</div>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-2.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
<script type="text/javascript" src="{{ asset('assets/js/richmarker-compiled.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/markerclusterer_packed.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/infobox.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.fitvids.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.trackpad-scroll-emulator.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/maps.js') }}"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.2/firebase.js"></script>
<script type="text/javascript" src="assets/js/moment-with-locales.js"></script>

<script>
    var optimizedDatabaseLoading = 0;
    var _latitude = 40.7344458;
    var _longitude = -73.86704922;
    var element = "map-homepage";
    var markerTarget = "sidebar"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
    var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
    var showMarkerLabels = false; // next to every marker will be a bubble with title
    var mapDefaultZoom = 14; // default zoom
    heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
</script>

@stack("scripts")
</body>

