<?php

// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------

// Remove this example in your live site -------------------------------------------------------------------------------

ob_start();
include 'data.php';

// This code can be used if you want to load data from JSON file

//$file_contents = file_get_contents("items.json");
//$data = json_decode($file_contents, true);

ob_end_clean();

// End of example ------------------------------------------------------------------------------------------------------

if( !empty($_POST['markers']) ){

    for( $i=0; $i < count($data); $i++){
        for( $e=0; $e < count($_POST['markers']); $e++){
            if( $data[$i]['id'] == $_POST['markers'][$e] ){
                echo
                '<div class="result-item" data-id="'. $data[$i]['id'] .'">';

                    // Ribbon ------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['ribbon']) ){
                        echo
                            '<figure class="ribbon">'. $data[$i]['ribbon'] .'</figure>';
                    }

                    echo
                    '<a href="'. $data[$i]['url'] .'">';

                    // chofer -------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['chofer']) ){
                        echo
                            '<h3>'. $data[$i]['chofer'] .'</h3>';
                    }

                    echo
                        '<div class="result-item-detail">';

                            // Foto del chofer -------------------------------------------------------------------------

                            if( !empty($data[$i]['imagen']) ){
                                echo
                                '<div class="image" style="background-image: url('. $data[$i]['imagen'] .')">';
                                    if( !empty($data[$i]['velocidad']) ){
                                        echo
                                        '<figure>'. $data[$i]['velocidad'] .'km/h</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<div class="price">'. $data[$i]['price'] .'</div>';
                                    }
                                echo
                                '</div>';
                            }
                            else {
                                echo
                                '<div class="image" style="background-image: url(assets/img/items/default.png)">';
                                    if( !empty($data[$i]['velocidad']) ){
                                        echo
                                        '<figure>'. $data[$i]['velocidad'] .'km.</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<figure class="price">'. $data[$i]['price'] .'</figure>';
                                    }
                                echo
                                '</div>';
                            }

                            echo
                            '<div class="description">';
                                if( !empty($data[$i]['ruta']) ){
                                    echo
                                        '<h5><i class="fa fa-map-marker"></i>Ruta: '. $data[$i]['ruta'] .'</h5>';
                                }

                                // Rating ------------------------------------------------------------------------------

                                if( !empty($data[$i]['rating']) ){
                                    echo
                                        '<div class="rating-passive"data-rating="'. $data[$i]['rating'] .'">
                                            <span class="stars"></span>
                                            <span class="reviews">'. $data[$i]['reviews_number'] .'</span>
                                        </div>';
                                }

                                // Category ----------------------------------------------------------------------------

                                if( !empty($data[$i]['category']) ){
                                    if ($data[$i]['category']=='Tanqueta') {
                                        echo
                                            '<div class="label label-default" style="background-color:#76BE43;color:#fff">'. $data[$i]['category'] .'</div>';
                                    }else{
                                        echo
                                            '<div class="label label-default" style="background-color:#4611A7;color:#fff">'. $data[$i]['category'] .'</div>';
                                        
                                    }
                                }

                                // Description -------------------------------------------------------------------------

                                if( !empty($data[$i]['distancia']) ){
                                    echo
                                        '<p>Recorrido (hoy): '. $data[$i]['distancia'] .'km.</p>';
                                }
                            echo
                            '</div>
                        </div>
                    </a>
                    <div class="controls-more">
                        <ul>
                            <li><a href="#" class="add-to-favorites">Add to favorites</a></li>
                            <li><a href="#" class="add-to-watchlist">Add to watchlist</a></li>
                        </ul>
                    </div>
                </div>';

            }
        }
    }

}