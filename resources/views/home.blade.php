@extends('layouts.app')

@section('content')
    <div class="hero-section full-screen has-map has-sidebar">
        <div class="map-wrapper">
            <div class="geo-location">
                <i class="fa fa-map-marker"></i>
            </div>
            <div class="map" id="map-homepage"></div>
        </div>
        <!--end map-wrapper-->
        <div class="results-wrapper">
            <div class="sidebar-detail">
                <div class="tse-scrollable">
                    <div class="tse-content">
                        <div class="sidebar-wrapper"></div>
                        <!--end sidebar-detail-content-->
                    </div>
                    <!--end tse-content-->
                </div>
                <!--end tse-scrollable-->
            </div>
            <!--end sidebar-detail-->
            <div class="results">
                <div class="tse-scrollable">
                    <div class="tse-content">
                        <div class="section-title">
                            <h2>Camiones<span class="results-number"></span></h2>
                        </div>
                        <!--end section-title-->
                        <div class="results-content"></div>
                        <!--end results-content-->
                    </div>
                    <!--end tse-content-->
                </div>
                <!--end tse-scrollable-->
            </div>
            <!--end results-->
        </div>
        <!--end results-wrapper-->



    </div>
    <!--end hero-section-->


    <div class="container"><hr></div>

    <section class="block">
        <div class="container">
            <div class="section-title">
                <h2>Events Near You</h2>
            </div>
            <!--end section-title-->
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="text-element event">
                        <div class="date-icon">
                            <figure class="day">22</figure>
                            <figure class="month">Jun</figure>
                        </div>
                        <h4><a href="detail.html">Lorem ipsum dolor sit amet</a></h4>
                        <figure class="date"><i class="icon_clock_alt"></i>08:00</figure>
                        <p>Ut nec vulputate enim. Nulla faucibus convallis dui. Donec arcu enim, scelerisque.</p>
                        <a href="detail.html" class="link arrow">More</a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
                <div class="col-md-4 col-sm-4">
                    <div class="text-element event">
                        <div class="date-icon">
                            <figure class="day">04</figure>
                            <figure class="month">Jul</figure>
                        </div>
                        <h4><a href="detail.html">Donec mattis mi vitae volutpat</a></h4>
                        <figure class="date"><i class="icon_clock_alt"></i>12:00</figure>
                        <p>Nullam vitae ex ac neque viverra ullamcorper eu at nunc. Morbi imperdiet.</p>
                        <a href="detail.html" class="link arrow">More</a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
                <div class="col-md-4 col-sm-4">
                    <div class="text-element event">
                        <div class="date-icon">
                            <figure class="day">12</figure>
                            <figure class="month">Aug</figure>
                        </div>
                        <h4><a href="detail.html">Vivamus placerat</a></h4>
                        <figure class="date"><i class="icon_clock_alt"></i>12:00</figure>
                        <p>Aenean sed purus ut massa scelerisque bibendum eget vel massa.</p>
                        <a href="detail.html" class="link arrow">More</a>
                    </div>
                    <!--end text-element-->
                </div>
                <!--end col-md-4-->
            </div>
            <!--end row-->
            <div class="background-wrapper">
                <div class="background-color background-color-black opacity-5"></div>
            </div>
            <!--end background-wrapper-->
        </div>
        <!--end container-->
    </section>
    <!--end block-->


    <!--end container-->


@endsection

@push("scripts")
    <script type="text/javascript">
        
            
    </script>
    
@endpush